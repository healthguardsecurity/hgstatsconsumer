options(stringsAsFactors = FALSE)

baseUrl <- "https://stats.cyberehr.net"

#' Get Data
#'
#' Parses data into human-readable format
#' @param response The response information to parse
#' @importFrom jsonlite fromJSON
#' @export
getData <- function(response){
  fromJSON(
    rawToChar(response$content)
  )
}

#' Get Analysis
#'
#' Runs analysis on the HealthGuard Stats service
#' @param path The endpoint you are accessing
#' @param body The parameters being passed to the process
#' @param url The location of the API
#' @param returnRaw Converts raw response to Obj
#' @importFrom httr POST
#' @export
getAnalysis <- function(path, body, url = baseUrl, returnRaw = FALSE){
  results <- POST(
    url = url,
    path = path,
    body = body,
    encode = "json"
  )
  if(!returnRaw){
    return(getData(results))
  }
  results
}

#' Get Fair Frequency
#' @param body bodyConfig
#' @param url URL
#' @param returnRaw Return Raw
#' @export
getFairFrequency <- function(body, url = baseUrl, returnRaw = FALSE){
  getAnalysis("fairFrequency", list(config=body), url, returnRaw)
}

#' Get Fair Taxonomy
#' @param frequency frequencyConfig
#' @param magnitude magnitudeConfig
#' @param url URL
#' @param returnRaw Return Raw
#' @export
getFairTaxonomy <- function(frequency, magnitude, url = baseUrl, returnRaw = FALSE){
  getAnalysis("fairTaxonomy", list(frequency=frequency, magnitude=magnitude), url, returnRaw)
}

#' Loss Config
#' Expects a primary magnitude distribution and a Secondary Loss pair of probability and magnitude distribution
#' @param primaryLoss Primary Loss Distribution
#' @param secondaryLoss populate with a secondaryLoss(probability, magnitude) result
#' @export
lossConfig <- function(primaryLoss, secondaryLoss){
  list(
    primaryMagnitude = primaryLoss,
    secondaryLoss = secondaryLoss
  )
}

#' Create SecondaryLoss
#' @param secondaryLossProbability Probability of secondary loss event
#' @param secondaryLossMagnitude Magnitude of secondary loss
#' @export
secondaryLoss <- function(secondaryLossProbability, secondaryLossMagnitude){
  list(
    secondaryFrequency = secondaryLossProbability,
    secondaryMagnitude = secondaryLossMagnitude
  )
}

#' Create Pert Config
#' @param min Min
#' @param max Max
#' @param mode Mode
#' @param shape Shape
#' @param iterations Iterations
#' @export
pertConfig <- function(min, max, mode, shape = 4, iterations = 1000){
  return(
    list(
      pertConfig = list(
        min = min,
        max = max,
        mode = mode,
        shape = shape,
        iterations = iterations
      )
    )
  )
}

#' Format SIPs
#' @param values The SIPs to be packaged
#' @export
SIPs <- function(values){
  list(sips=values)
}

#' Add Percentage
#' @param value Percentage likelihood of event
#' @export
percentage <- function(value){
  list(probability=value)
}
